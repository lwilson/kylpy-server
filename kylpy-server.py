from flask import Flask
import json

app = Flask(__name__)

data = {}

with open("asu.json", "r") as f:
    data = json.load(f)

def save():
    with open("asu.json", "w") as f:
        json.dump(data, f, indent=2)


@app.route("/api/v1/asu")
def getData():
    return json.dumps(data)

@app.route("/api/v1/asu/<id>")
def getEntry(id):
    return json.dumps(data[id])

@app.route("/api/v1/asu/<id>/review/<int:val>")
def review(id, val):
    data[id]["rating"]["t"] += val
    data[id]["rating"]["n"] += 1
    save()
    return "done"

@app.route("/api/v1/asu/<id>/unreview/<int:val>")
def unreview(id, val):
    data[id]["rating"]["t"] -= val
    data[id]["rating"]["n"] -= 1
    save()
    return "done"
